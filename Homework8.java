
public class Homework8 {
    public static void main(String[] args) {

        BankingAccount myAccount = new BankingAccount();

        myAccount.setBalance(0);

        myAccount.deposit(500);
        myAccount.deposit(150);
        myAccount.deposit(35);

        myAccount.withdraw(40);
        myAccount.withdraw(120);
        myAccount.withdraw(900); //withdraw attempt that's over the balance



    }

}