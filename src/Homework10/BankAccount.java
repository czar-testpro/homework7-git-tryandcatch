package src.Homework10;

public class BankAccount {

    public int balance = 0;
    public int getBalance() {
        return balance;
    }
    public void setBalance(int balance) {

        if (balance>0||balance==0) {
            System.out.println("Balance is not negative");
        }
        else{
            System.out.println("Balance is negative");
        }
        this.balance = balance;

    }

    public void deposit(int amount)
    {
        int newBalance = balance + amount;
        balance = newBalance;
        System.out.println("Your current balance is $"+getBalance());
    }

    public void withdraw(int amount)
    {
        if (balance > amount) {
            int newBalance = balance - amount;
            balance = newBalance;
            System.out.println("Your balance has changed by $"+amount+" and now it is $" +getBalance());
        }
        else{
            System.out.println("Withdrawal of $"+amount+" cannot be completed. Your balance is $"+balance+". Do not change the balance!");
        }

    }




}
