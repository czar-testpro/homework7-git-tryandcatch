import java.util.Arrays;
import java.util.Scanner;

public class JavaHomeworkTryCatch {

    public static void main(String[] args) {

        int [] firstArray = {1,2,3};

        Scanner input = new Scanner(System.in);

            try {
                System.out.println("Please enter the index of the array to display its value: ");
                int arrayIndex = input.nextInt();
                System.out.println("The array value at index " +arrayIndex+ " is " +firstArray[arrayIndex]);

            } catch (Exception e) {
                System.out.println("This is why QA Engineers always have to do boundary testing! The array only has 3 values and you've requested a 4th");
            }
    }
}
